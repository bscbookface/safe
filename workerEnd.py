#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Worker code for endpoint, writing to database"""
import pika
import sys
import json
import pymongo
from pymongo import MongoClient
import time
import yaml

#open config file
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

#Connect to database
try:
    dbCon = MongoClient('mongodb://' +cfg['databaseIP'] + ':' + \
                        cfg['databasePort'] + '/')
    db = dbCon[cfg['db']]
    posts = db[cfg['collection']]
    collection_exist = db.collection_names()
except pymongo.errors.ServerSelectionTimeoutError as err:
    print " [!]  Could not connect to DB: %s" % err
    print " [!]  No items will be written to DB!!"
    sys.exit()

#Establish pika connection to rabbitmq
credentials = pika.PlainCredentials(cfg['rabbitUser'], cfg['rabbitPsw'])
parameters = pika.ConnectionParameters(host='localhost')
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

#declare a durable queue
channel.queue_declare(queue=cfg['queue'], durable=True)

def time_stamp():
    """ Function for getting timestamp"""
    stamp = time.ctime(int(time.time()))
    return stamp

#Function that consumes messages
def callback(ch, method, properties, body):
    """ Function for consuming messages from queue"""
    print " [*] " + time_stamp() + " Accepted job"
    data = json.loads(body)
    time.sleep(2)
    data["timeStop"] = time_stamp()
    if not collection_exist:
        data["id"] = 1
    else:
        try:
            get = posts.find({}, {"id":1, "_id":0})\
                    .sort([("_id", pymongo.DESCENDING)]).limit(1)
            for doc in get:
                try:
                    val = int(doc['id']) +1
                    data["id"] = val
                except KeyError:
                    print " [!] " + time_stamp() + \
                        "Error in database! ID field not found"
                    sys.exit()
        except pymongo.errors.ConnectionFailure as err:
            print " [!] " + time_stamp() + \
                    " Could not get ID from database!! %s" % err
            sys.exit()
    try:
        posts.insert_one(data)
        print(" [*] Wrote to database, ID: {}".format(data['id']))
    except pymongo.errors.ConnectionFailure as err:
        print " [!] " + time_stamp() + \
                " Could not post to database! %s" % err
        sys.exit()
    channel.basic_ack(delivery_tag=method.delivery_tag)
    print " [*] " + time_stamp() + " Job finished"
    print " [*] " + time_stamp() + " Waiting for next job"

print " [*] " + time_stamp() + " Waiting for job"

channel.basic_consume(callback, queue=cfg['queue'])
channel.start_consuming()
