#WorkerEnd application

built with pika,pyyaml,pymongo

Installation:
 1. Clone from git
 2. cd worker/
 3. pip install -r requirements.txt
 4. chmod +x workerEnd.py
 5. edit config.yaml
 6. ./workerEnd.py

Recommended to use with PM2
